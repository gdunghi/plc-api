package part

import (
	"net/http"

	"github.com/labstack/echo"
)

// GetPartBySymptom Handler
func GetPartBySymptomHandler(c echo.Context) error {
	parts, _ := GetPartBySymptomID(1)
	return c.JSON(http.StatusOK, parts)
}
