package part

import (
	"github.com/gdunghi/plc-api/database"
)

func GetPartBySymptomID(symptomID int64) ([]Part, error) {
	db := database.Connect()
	defer db.Close()
	var parts []Part

	err := db.Table("parts").Select("parts.*").
		Joins("inner  JOIN  symptom_details detail on parts.id = detail.part_id").
		Joins("inner join symptom on symptom.id = detail.symptom_id").Where("symptom.id =  ?", symptomID).Find(&parts).Error

	return parts, err
}
