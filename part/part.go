package part

//Part ... Part model
type Part struct {
	ID          int    `json:"id" db:"id"`
	PartNo      string `json:"partNo"  db:"part_no"`
	PartName    string `json:"partName" db:"part_name"`
	PartPrice   string `json:"artPrice" db:"part_price"`
	Color       string `json:"color" db:"color"`
	CreatedBy   string `json:"createdBy" db:"created_by"`
	CreatedDate string `json:"createdDate" db:"created_date"`
	UpdatedBy   string `json:"updatedBy" db:"updated_by"`
	UpdatedDate string `json:"updatedDate" db:"updated_date"`
}
