package symptom

//Symptom ... Symptom model
type Symptom struct {
	ID             int    `json:"id" db:"id"`
	SymptomCode    string `json:"symptomCode"  db:"symptom_code"`
	SymptomName    string `json:"symptomName" db:"symptom_name"`
	EstimatePrice  string `json:"estimatePrice" db:"estimate_price"`
	SymptomDetails string `json:"symptomDetails" db:"color"`
	CreatedBy      string `json:"createdBy" db:"created_by"`
	CreatedDate    string `json:"createdDate" db:"created_date"`
	UpdatedBy      string `json:"updatedBy" db:"updated_by"`
	UpdatedDate    string `json:"updatedDate" db:"updated_date"`
}
