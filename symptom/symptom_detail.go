package symptom

//SymptomDetail ... SymptomDetail model
type SymptomDetail struct {
	ID          int    `json:"id" db:"id"`
	SymptomID   string `json:"symptomId"  db:"symptom_id"`
	PartID      string `json:"sartId" db:"part_id"`
	CreatedBy   string `json:"createdBy" db:"created_by"`
	CreatedDate string `json:"createdDate" db:"created_date"`
	UpdatedBy   string `json:"updatedBy" db:"updated_by"`
	UpdatedDate string `json:"updatedDate" db:"updated_date"`
}
