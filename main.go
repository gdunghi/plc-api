package main

import (
	"log"

	"github.com/gdunghi/plc-api/database"
	"github.com/gdunghi/plc-api/part"
	"github.com/joho/godotenv"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {

	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/", part.GetPartBySymptomHandler)

	db := database.Connect()
	defer db.Close()

	db.DB().Ping()

	// Start server
	e.Logger.Fatal(e.Start(":1323"))
}

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file", err)
	}
}
